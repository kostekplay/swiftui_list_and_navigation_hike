////  HikeDetail.swift
//  SwiftUIListAndNavigationHike
//
//  Created on 12/11/2020.
//  
//

import SwiftUI

struct HikeDetail: View {
    
    let hike: Hike
    
    @State private var zoomed: Bool = false
    
    var body: some View {
        VStack {
            Image(hike.image)
                .resizable()
                .aspectRatio(contentMode: zoomed ? .fit : .fill)
                .padding(10.0)
                .onTapGesture{
                    withAnimation {
                        zoomed.toggle()
                        print(zoomed)
                    }
                }
            
            Text(hike.name)
                .padding(10.0)
            Text(String(format: "%.2f", hike.mil) + " mil")
        }
        .padding(10.0)
        .navigationBarTitle(hike.name, displayMode: .inline)
    }
}

struct HikeDetail_Previews: PreviewProvider {
    static var previews: some View {
        HikeDetail(hike: Hike(name: "Jacek", image: "jacek", mil: 25))
    }
}
