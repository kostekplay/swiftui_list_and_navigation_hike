////  Hike.swift
//  SwiftUIListAndNavigationHike
//
//  Created on 11/11/2020.
//  
//

import Foundation

struct Hike {
    
    let name: String
    let image: String
    let mil: Double
    
}

extension Hike {
    
    static func all() -> [Hike] {
        
        return [
            
            Hike(name: "Jacek", image: "jacek", mil: 10),
            Hike(name: "Iza", image: "iza", mil: 15),
            Hike(name: "Kazik", image: "kazik", mil: 20)
                
        ]
    }
    
}
