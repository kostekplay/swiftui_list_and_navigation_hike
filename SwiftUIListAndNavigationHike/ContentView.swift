////  ContentView.swift
//  SwiftUIListAndNavigationHike
//
//  Created on 11/11/2020.
//  
//

import SwiftUI

struct ContentView: View {
    
    let hikes = Hike.all()
    
    var body: some View {
        NavigationView{
            List (hikes, id:\.name){ hike in
                NavigationLink( destination: HikeDetail(hike: hike)){
                    HikeCell(hike: hike)
                }
            }
            .navigationBarTitle("Huking")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct HikeCell: View {
    
    let hike: Hike
    
    var body: some View {
        HStack {
            Image(hike.image)
                .resizable()
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: 75, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .cornerRadius(12)
            VStack(alignment: .leading){
                Text(hike.name)
                Text(String(format: "%.2f", hike.mil) + " mil")
            }
            
        }
    }
}
